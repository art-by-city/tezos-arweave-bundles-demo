import ArLocal from 'arlocal'
import Arweave from 'arweave'
import TezosBundler from 'tezos-bundler'

import testweaveJWK from './testweave-keyfile.json'

const ARWEAVE_PROTOCOL = process.env.ARWEAVE_PROTOCOL || 'http'
const ARWEAVE_HOST = process.env.ARWEAVE_HOST || 'localhost'
const ARWEAVE_PORT = process.env.ARWEAVE_PORT
  ? Number.parseInt(process.env.ARWEAVE_PORT)
  : 1984

const arweave = new Arweave({
  protocol: ARWEAVE_PROTOCOL,
  host: ARWEAVE_HOST,
  port: ARWEAVE_PORT
})
const tezosBundler = new TezosBundler(testweaveJWK, arweave as any)
const callback = tezosBundler.app.callback()

export default callback
