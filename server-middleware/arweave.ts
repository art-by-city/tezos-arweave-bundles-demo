import Koa from 'koa'
import Router from '@koa/router'
import Arweave from 'arweave'
import ArDB from 'ardb'
import ArdbTransaction from 'ardb/lib/models/transaction'
import axios from 'axios'

const arweaveConfig = {
  protocol: process.env.ARWEAVE_PROTOCOL || 'http',
  host: process.env.ARWEAVE_HOST || 'localhost',
  port: process.env.ARWEAVE_PORT || 1984
}
const gateway =
  `${arweaveConfig.protocol}://${arweaveConfig.host}:${arweaveConfig.port}`
const appName = process.env.EVENT_NAME
  || 'Art By City - Tezos Arweave Bundles Demo'
const app = new Koa()
const router = new Router()

router.get('/bundle-messages', async (ctx) => {
  await axios.get(`${gateway}/mine`)

  const arweave = new Arweave(arweaveConfig)
  const ardb = new ArDB(arweave, 2)

  let query = ardb
    .search('transactions')
    .appName(appName)
    .tag('App-Version', '1.0.0')
    .type('application/json')

  const transactions =
    await query.find({ sort: 'HEIGHT_DESC' }) as ArdbTransaction[]

  console.log(
    '[Arweave Server Middleware] got message transactions',
    transactions.length
  )

  const messages = await Promise.all(transactions.map(async tx => {
      const res: any = await axios.get(`${gateway}/${tx.id}`)
      const message = res.data.message

      const externalOwner = tx.tags.find(tag => tag.name === 'External-Owner')
      const externalNetwork = tx.tags.find(
        tag => tag.name === 'External-Network'
      )

      return {
        messageId: tx.id,
        message,
        signature: tx.signature,
        owner: externalOwner ? externalOwner.value : '',
        network: externalNetwork ? externalNetwork.value : ''
      }
    }))

  ctx.body = messages

  return
})

app
  .use(async (ctx, next) => {
    ctx.set('Access-Control-Allow-Origin', '*')

    await next()
  })
  .use(router.routes())
  .use(router.allowedMethods())

export default app.callback()
