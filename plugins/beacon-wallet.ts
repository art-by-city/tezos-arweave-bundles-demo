import { Context } from '@nuxt/types'
import { Inject } from '@nuxt/types/app'
import { BeaconWallet } from '@taquito/beacon-wallet'
import { DAppClientOptions } from '@airgap/beacon-dapp'

export class BeaconWalletPlugin {
  config!: DAppClientOptions
  wallet: BeaconWallet

  constructor(config: DAppClientOptions) {
    this.config = config
    this.wallet = BeaconWalletPlugin.buildNewWallet(this.config)
  }

  async disconnect(): Promise<void> {
    await this.wallet.disconnect()
    this.wallet = BeaconWalletPlugin.buildNewWallet(this.config)
  }

  static buildNewWallet(config: DAppClientOptions): BeaconWallet {
    return new BeaconWallet(config)
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $beacon: BeaconWalletPlugin
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $beacon: BeaconWalletPlugin
  }

  interface Context {
    $beacon: BeaconWalletPlugin
  }
}

declare module 'vuex/types/index' {
  interface Store<S> {
    $beacon: BeaconWalletPlugin
  }
}

export default ({ $config }: Context, inject: Inject) => {
  inject('beacon', new BeaconWalletPlugin({
    name: $config.dapp.name,
    iconUrl: $config.dapp.iconUrl,
    preferredNetwork: $config.tezos.network
    // NB: Sometimes extension doesn't respond if this is used?
    // disableDefaultEvents: true,
  }))
}
