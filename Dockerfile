FROM node:16-alpine

ADD . .
RUN npm install

EXPOSE 8080
ENV HOST=0.0.0.0
ENV PORT=8080

RUN npm run build

ENTRYPOINT npm run start:demo
